
public enum Audio {
	
	MP3("mp3",128) {

		@Override
		public String reproduce(String archive) {

			return archive+" > file MP3";
		}
		
	},
	
	PCM("PCM"){
		
		@Override
		public String reproduce(String archive) {
			return archive+" > file PCM";
		}
	
	},
	DD("Dolby Digital",256) {

		@Override
		public String reproduce(String archive) {

			return archive+" > file Dolby Digital";
		}

	};
	
	
	@Override
	public String toString() {
		return "Dolby";
	}
	
	
	
	private final String channel;
	private final int bitrate;
	
	private Audio(String channel, int bitrate) {
		this.channel=channel;
		this.bitrate=bitrate;
	}

	private Audio(String channel) {
		this.channel=channel;
		bitrate = -1;
		}
	
	
	public abstract String reproduce(String archive);
	
	public String getChannel() {
		return channel;
		}
		public int getBitrate() {
		return bitrate;
		}
	
}
