
public class Main {

	public static void main(String[] args) {

		// values � metodo statico che ritorna un array di tutti i possibili valori
		for ( Giorno d : Giorno.values()) {
			System.out.println(d);
		}
		
		//convertire stringhe in valori del nostro enum
		Giorno g = Giorno.valueOf("SABATO");
		
		System.out.println();
		System.out.println("il valore di 'g' � " + g);
	
	
		/////////////////////////////////////////////
		
		for ( Elemento e : Elemento.values() ) {
			System.out.println(e.getNumeroAtomico());
		}
		
		
		for( Audio a : Audio.values() ) {
			System.out.printf("%s\t| %d\t| %s\t | %s\n", a, a.getBitrate(), a.getChannel(), a.reproduce("myFile"));
			}
		
	
	
	}
	
	

}
